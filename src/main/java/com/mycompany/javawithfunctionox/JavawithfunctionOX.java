/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.mycompany.javawithfunctionox;

/**
 *
 * @author nutty
 */
import java.util.Scanner;

public class JavawithfunctionOX {

    static void printWelcomeOX() {
        System.out.println("Welcome To OX");
    }
    static char[][] table = {{'-', '-', '-'},
    {'-', '-', '-'},
    {'-', '-', '-'}};

    static char currentPlayer = 'X';

    static void printTurn() {
        System.out.println(currentPlayer + " Turn");
    }

    static void printTable() {
        for (int i = 0; i < 3; i++) {
            for (int x = 0; x < 3; x++) {
                System.out.print(table[i][x]);
            }
            System.out.println("");
        }
    }
    static int Row, Col;

    static boolean isWin(){
        if(checkRow()||checkCol()||checkDiagonal1()||checkDiagonal2()){
            return true;
        }
        return false;
    }

    static boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (table[Row - 1][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }
    static boolean checkCol(){
        for(int i=0; i<3; i++){
            if(table[i][Col-1]!=currentPlayer){
                return false;
            }
        }
        return true;  
    }

    static boolean isDraw() {
        int blank = 0;
        for(int i=0; i<3; i++){
            for(int j=0; j<3; j++){
                if(table[i][j]!='-'){
                    blank+=1;
                }
            }
        }
        if(blank==9){
            return true;
        }
        return false;
    }
    static void showResult(){
        if(isDraw()){
            System.out.println("Draw!");
        }else{
            System.out.println(currentPlayer+" win!");
        }

    }

    static void printWin() {
        System.out.println(currentPlayer + " win!");
    }

    static void inputRowColum() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.println("Plese input Row and Col :");
            Row = sc.nextInt();
            Col = sc.nextInt();
            if (table[Row - 1][Col - 1] == '-') {
                table[Row - 1][Col - 1] = currentPlayer;
                break;
            }
        }

    }

    static void switchPlayer() {
        if (currentPlayer == 'X') {
            currentPlayer = 'O';
        } else {
            currentPlayer = 'X';
        }
    }
    static boolean checkDiagonal1(){
        if(table[0][0]==currentPlayer&&table[1][1]==currentPlayer&&table[2][2]==currentPlayer){
            return true;
        }
        return false;
    }
    
    static boolean checkDiagonal2(){
        if(table[0][2]==currentPlayer&&table[1][1]==currentPlayer&&table[2][0]==currentPlayer){
            return true;
        }
        return false;
    }
    static boolean inputContinue(){
        Scanner kb = new Scanner(System.in);
        System.out.print("continue(y/n) : ");
        char isCon = kb.next().charAt(0);
        if(isCon=='y'){
            return true;
        }
        return false;
    }
    static void reTable(){
        for(int i=0; i<3; i++){
            for(int j=0; j<3; j++){
                table[i][j] = '-';
            }
        }
    }

    public static void main(String[] args) {
        printWelcomeOX();
        while(true){
            while(true){
                printTable();
                printTurn();
                inputRowColum();
                if(isWin()){
                    printTable();
                    showResult();
                    break;
                }
                if(isDraw()){
                    printTable();
                    showResult();
                    break;
                }
                switchPlayer();
            }
            if(inputContinue()){
                reTable();
            }else{
                break;
            }
        }
    }
}
